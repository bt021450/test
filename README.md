```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iso646.h>
#include <string.h>

void sort(int base[], int length);
void print_list(int base[], int length);
void test(int base[], int length);
void tests();
void benchmarks();

/* Bubble sort implementation */
void sort(int list[], int size)
{
    for(int j=1;j<size;j++)
    {
        int key=list[j];
        int i = j-1;
        while(i>-1 and list[i]>key)
        {
            list[i+1]=list[i];
            i=i-1;
        }
        list[i+1]=key;

    }
}


/* Print a list to standard output. */
void print_list(int base[], int length) {
    for (int n = 0; n < length; n++) {
        printf("%d ", base[n]);
    }
    printf("\n");
}

/* Print a list, sort it, then print it again. */
void test(int base[], int length) {
    printf("Before sorting: ");
    print_list(base, length);
    sort(base, length);
    printf("After sorting:  ");
    print_list(base, length);
}

/* Run some tests of the sorting algorithm. */
void tests() {
    /* Basic test to check if a list can be sorted correctly. */
    printf("Basic test\n");
    int test0[] = {0, 1, 5, -4, -8, 3, 2, 9, -7, 6};
    test(test0, sizeof(test0)/sizeof(int));
    printf("\n");
    
    /* Test case 1: Sorting an empty list with the length of 0. */
    printf("Empty list length of 0 test\n");
    int test_case_1[] = {};
    test(test_case_1, sizeof(test_case_1)/sizeof(int));
    printf("\n");
    
    /* Test case 2: Sorting a list with the length of 1. */
    printf("List length of 1 test\n");
    int test_case_2[] = {4};
    test(test_case_2, sizeof(test_case_2)/sizeof(int));
    printf("\n");
    
    /* Test case 3: Sorting a list with the length of 2. */
    printf("List length of 2 test\n");
    int test_case_3[] = {9, 4};
    test(test_case_3, sizeof(test_case_3)/sizeof(int));
    printf("\n");
    
    /* Test case 4: Sorting a list with at least 2 equal elements. */
    printf("List with equal elements test\n");
    int test_case_4[] = {4, 2, 5, 9, 3, 7, 7, 2, 9, 2, 5, 8};
    test(test_case_4, sizeof(test_case_4)/sizeof(int));
    printf("\n");
}

/* Benchmark the sorting algorithm. */
void benchmarks() {
    /* Benchmark Code */
    srand ( time(NULL) );
    int *my_array_1;
    int *my_array_2;
    int i;
    int maxbench = 1000000;
    my_array_1 = (int *)malloc(sizeof(int)*maxbench);
    printf("Length, Duration in Milliseconds\n");
    for(i=0; i<maxbench; i++){
            my_array_1[i] = rand();
            if(!(i % 10000) && i != 0){
                struct timespec start, end;
                clock_gettime(CLOCK_MONOTONIC, &start);
                sort(my_array_1, i);
                clock_gettime(CLOCK_MONOTONIC, &end);
                uint64_t delta_ms = ((end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000) / 1000;
                printf("%d, ", i);
                printf("%d\n", delta_ms);
            }
            
        
    }


}

int main() {
    tests();
    benchmarks();
    exit(0);
}
```